# import needed libraries
import pandas as pd
import numpy as np
import pandas_usaddress
import statistics as stats
#import datetime as dt

# terinology = feature for columns


def main():
    # read in data and store in pandas dataframe
    df_account = pd.read_csv('Data/Feature Engineering Data - Account - Parent.csv')
    df_opportunity = pd.read_csv('Data/Feature Engineering Data - Opportunity - Child.csv')
    # print(df_account.head())                                                                   #debug
    # Method Calls
    # null_missing_values(df_opportunity)
    # consolidate_cat_vars(df_opportunity)
    # quant_mathematical_trans(df_account)
    # parse_address(df_account)
    # quant_var_binning(df_account)
    #sum_of_opps_for_acct(df_opportunity, df_account)
    # avg_of_opp_for_acct(df_opportunity)
    #age_of_record(df_account)
    #time_between_dates(df_opportunity)
    #binning_opp_dates(df_opportunity)
    #binning_acct_dates(df_account)
    num_opps_in_stage(df_opportunity, df_account)




def num_opps_in_stage(df_opp, df_account):
    #print(df_opp['Stage'].value_counts())
    df = df_opp.reset_index().groupby(['Account Name', 'Account ID'])['Stage'].value_counts()
    print(df)
    # temp = df_opp.reset_index().groupby(['Account Name', 'Account ID'])['Stage'].value_counts()
    # temp = df_opp.reset_index().groupby(['Account Name', 'Account ID', 'Stage'], as_index=False).count()
    #df = pd.DataFrame(df_opp.groupby(['Account Name', 'Account ID'])).reset_index()
    #print(list(df.columns))
    
    # df = pd.DataFrame(temp)
    print(df)
    #print(df['Stage'].value_counts())
    # test['count'] = 'hold'
    # for i in range(0, df.shape[0]):
    #     #print(i)
    #     test['count'][i] = df['Stage'][i]
        #print(df['Stage'][i])
    #print(df['Stage'][0])
    #df.pivot(index='Account Name', columns='Stage')
    print(list(df.columns))
    #print(df)
    df.to_csv('groupby.csv')
    

def binning_acct_dates(df_account):
    # old record vs new records cut off date: '2019-01-01 00:00:00'
    df_account['Account_Created'] = pd.to_datetime(df_account['Account_Created'], infer_datetime_format=True)
    df_account['old_new'] = pd.cut(df_account['Account_Created'], 2, labels=['old','new'])
    
    cut_off_temp = '2019-01-01 00:00:00'
    cut_off = pd.to_datetime(cut_off_temp, infer_datetime_format=True)
    print(cut_off)
    df_account['if_old_new'] = 'temp'
    for i in range(df_account.shape[0]):
        if (df_account['Account_Created'][i] < cut_off):
            df_account['if_old_new'][i] = 'old'
        else:
            df_account['if_old_new'][i] = 'new'
    
    df_account.to_csv('old_new.csv')

def binning_opp_dates(df_opportunity):
    # https://www.geeksforgeeks.org/python-pandas-series-dt-quarter/
    df_opportunity['Created Date'] = pd.to_datetime(df_opportunity['Created Date'], infer_datetime_format=True)
    df_opportunity['quarter'] = df_opportunity['Created Date'].dt.quarter
    df_opportunity.to_csv('quart.csv')
    # bin by quarter - ignoring year
    
def age_of_record(df_account):
    # convert date rows in dataframe to datatime type
    # Account_Created
    df_account['Account_Created'] = pd.to_datetime(df_account['Account_Created'], infer_datetime_format=True)
    df_account['record_age'] = (pd.datetime.today() - df_account['Account_Created'])
    #print(df_account.record_age.dt.day.head())
    #df_account['record_age'] = df_account['record_age'] / 2
    #df_account['record_age'] = df_account['record_age'].values.astype('datetime64[D]')
    df_account['record_age'] = df_account['record_age'].astype('timedelta64[D]')
    # print(df_account['record_age'].dtype)
    #df_account['record_age'].to_Integer
    #print(df_account['record_age'].dtype)
    # print(df_account.head())
    df_account.to_csv('acct_date_calcs.csv')
    # print(s)

def time_between_dates(df_opportunity):
    # convert date rows in dataframe to datatime type
    # Closed Date
    df_opportunity['Closed Date'] = pd.to_datetime(df_opportunity['Closed Date'], infer_datetime_format=True)
    # Last Modified Date
    df_opportunity['Last Modified Date'] = pd.to_datetime(df_opportunity['Last Modified Date'], infer_datetime_format=True)
    # Created Date
    df_opportunity['Created Date'] = pd.to_datetime(df_opportunity['Created Date'], infer_datetime_format=True)
    
    # time to close
    df_opportunity['created_to_closed'] = df_opportunity['Closed Date'] - df_opportunity['Created Date']
    # convert to weeks
    df_opportunity['created_to_closed'] = df_opportunity['created_to_closed'].astype('timedelta64[W]')
    #df_opp['created_to_closed'] = pd.DatetimeIndex( df_opp['created_to_closed']).week
    #print( df_opp['created_to_closed'])
    # time since last modified
    df_opportunity['last_mod'] = pd.datetime.today() - df_opportunity['Last Modified Date'] 
    print(pd.datetime.today())
    #convert to hours 
    df_opportunity['last_mod'] = df_opportunity['last_mod'].astype('timedelta64[h]')
    #df_opp['last_mod'] = pd.DatetimeIndex(df_opp['last_mod']).hour
    #print(df_opp['last_mod'])
    df_opportunity.to_csv('datetime_calcs.csv')

def null_missing_values(df_opp):
    # on opprotunity record
    # newer records missing description or amount
    # closed date --> open opps have missing closed dates
    print("identify missing values")
    # print(df_account)
    print(df_opp.isnull().sum())

def num_in_cat():
    # number 14
    pass

def consolidate_cat_vars(df_opportunity):
    # TODO: add description of if statment approach
    df_opportunity['if_binned'] = 'default'
    # loop through aa opportunity records
    for i in range(df_opportunity.shape[0]):
        # if stage containes closed won or closed lost
        if(df_opportunity['Stage'].str.contains('Closed Lost')[i] | df_opportunity['Stage'].str.contains('Closed Won')[i]):
            # set binned value to closed
            df_opportunity['if_binned'][i] = 'Closed'
        # if stage contains prospecting
        elif(df_opportunity['Stage'].str.contains('Prospecting')[i]):
            # set binned value to new
            df_opportunity['if_binned'][i] = 'New'
        else:
            # otherwise set binned value it in progress
            df_opportunity['if_binned'][i] = 'In Progress'

    # TODO: provide description for streamlined approach using pandas and numpy
    df_opportunity['binned_stage'] = np.where(df_opportunity['Stage'].str.contains('Closed'), 'Closed', np.where(df_opportunity['Stage'].str.contains('Prospecting'), 'New', 'In Progress'))

    # df_opp.to_csv('binned.csv')                                                                            # Send to csv for sanity check/debug

def quant_var_binning(df_account):
    test = pd.DataFrame(df_account)
    # print(test1.shape)
    test['if_Binned'] = '--'
    for i in range(test.shape[0]):
        # print(test1['Number_of_Employees'][i].dtype)
        if (test['Number_of_Employees'][i] <= 25000):
            # print('SMALL')
            test['if_Binned'][i] = 'Small'
        elif (test['Number_of_Employees'][i] > 25000 and test['Number_of_Employees'][i] <= 100000):
            # print('MED')
            test['if_Binned'][i] = 'Medium'
        elif (test['Number_of_Employees'][i] > 100000):
            # print('LARGE')
            test['if_Binned'][i] = 'Large'
        else:
            print('error')

    # bin based on quantiles
    test['Qcut_Binned'] = pd.qcut(df_account['Number_of_Employees'], 3, labels=[
                                  'Small', 'Medium', 'Large'])

    # define your bin range
    cut_lables = ['Small', 'Medium', 'Large']
    cut_bins = [0, 25000, 100000, np.inf]
    test['cut_Binned'] = pd.cut(
        df_account['Number_of_Employees'], cut_bins, labels=cut_lables)

    # sanity check
    test.to_csv('qcut.csv')

def sum_of_opps_for_acct(df_opp, df_account):
    df = pd.DataFrame(df_opp)
    temp = df.groupby('Account ID').agg(val_sum=('Amount', sum))
    temp_df = pd.DataFrame(temp)

    temp_df.to_csv('agg_sum.csv')

def avg_of_opp_for_acct(df_opp):
    df = pd.DataFrame(df_opp)
    df = df.groupby('Account ID').agg(val_sum=('Amount', np.mean))

    df.to_csv('agg_avg.csv')

def quant_mathematical_trans(df_account):
    # print(df_account['Number_of_Employees'])
    # take nature log
    df_account_trans = df_account.assign(
        log_density=np.log(df_account['Number_of_Employees']))

    # take exponent of natural log
    df_account_trans['exp_num_prod'] = np.exp(df_account['Number_of_Products'])

    # take square root of --> num of emp
    df_account_trans['sqrt_of_emp'] = np.sqrt(
        df_account['Number_of_Employees'])

    # power transformation (^2) --> locations
    df_account_trans['pow_num_locations'] = np.power(
        df_account['Number_of_Locations'], 2)

    # standardizing ( (x-mean(x)) / var(x) ) --> num of employees
    num_emp = df_account['Number_of_Employees']
    mean = np.mean(df_account['Number_of_Employees'])
    std_POP = np.std(df_account['Number_of_Employees'])
    st_samp = stats.stdev(df_account['Number_of_Employees'])
    print('mean', mean)
    print('std', st_samp)
    df_account_trans['stadardize_num_emp'] = (
        (df_account['Number_of_Employees'] - mean)/st_samp)

    # range from 0 to 1 (x-min(x))/(max(x)-min(x)) --> anual rev
    df_account_trans['normalized_emp'] = ((df_account['Number_of_Employees'] - df_account['Number_of_Employees'].min())/(
        df_account['Number_of_Employees'].max()-df_account['Number_of_Employees'].min()))

    df_account_trans.to_csv('acct_trans.csv')

def parse_address(df_account):
    df = pandas_usaddress.tag(
        df_account, ['Shipping_Address'], granularity='medium', standardize=True)
    df = df.drop(['Industry', 'Year_Started', 'Account_Created', 'Billing_Address', 'City', 'Number_of_Employees', 'Website',
                  'Phone', 'Account_Owner', 'Annual_Revenue', 'Number_of_Locations', 'Number_of_Products', 'Descriptions'], axis=1)
    df.to_csv('parsed_address.csv')


main()
